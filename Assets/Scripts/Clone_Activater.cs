using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clone_Activater : MonoBehaviour
{
    public bool SpawnCloneFirstActivation;

    public bool DestroyAfterActivation;

    public bool NeedPPE = true;

    public void ActivatedByPlayer()
    {
        
        if (NeedPPE)
        {
            if (Interact_Controller.Instance.HasInsulationGloves)
            {
                if (SpawnCloneFirstActivation)
                {
                    Game_Controller.Instance.ShowPreCloneScreen();
                    SpawnCloneFirstActivation = false;
                }

                else
                {
                    Game_Controller.Instance.CompletedLevel();
                    UI_Controller.Instance.ActivateWinScreen();
                }
                
            }

            else
            {
                Game_Controller.Instance.Electrocuted();
                UI_Controller.Instance.ActivateElectrocuteScreen();
            }
        }
      
        else
        {
            if (SpawnCloneFirstActivation)
            {
                Game_Controller.Instance.ShowPreCloneScreen();
                SpawnCloneFirstActivation = false;
            }

            else
            {
                Game_Controller.Instance.CompletedLevel();
                UI_Controller.Instance.ActivateWinScreen();
            }
        }

        

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToMenu : MonoBehaviour
{
    // Start is called before the first frame update

    public int WaitTime = 25;
    public GameObject WhatToRemove;
    float T;
    void Start()
    {
        if (WhatToRemove == null)
            WhatToRemove = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
         T += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.N) && T > WaitTime)
        {
            WhatToRemove.SetActive(false);
        }
    }
}

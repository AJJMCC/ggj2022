using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClonePositionRecorder : MonoBehaviour
{

    public GameObject PlayerObject;
    public GameObject CloneObjectPrefab;

    public Transform SpawnPosition;

    public bool OnlyOneClone = true;


    // Clone Objects to keep track of
    private GameObject Instantiated_Clone;
    bool ShouldRecordMovements = false;
    public bool ReplayMovements = false;
    List<TimeData> m_playerTimeData = new List<TimeData>();

   


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (ShouldRecordMovements)
        {
            // If we're at our limit we will remove old recorded time data.
            if (m_playerTimeData.Count > 7200)
            {
                m_playerTimeData.RemoveAt(m_playerTimeData.Count - 1);
            }
            RecordPlayer();
        }

        if (ReplayMovements)
        {
            if (m_playerTimeData.Count > 0)
            {
                var timeData = m_playerTimeData[0];
                GiveGameObjectData(timeData);
                m_playerTimeData.RemoveAt(0);
               // Debug.Log("Started replaying data");
            }
            else
            {
                ReplayMovements = false;
                Debug.Log("End of replay file");
                Destroy(Instantiated_Clone);
                Instantiated_Clone = null;
            }
        }
    }

    private void RecordPlayer()
    {
        var newTimeDataRecording = new TimeData(PlayerObject.transform.position, PlayerObject.transform.eulerAngles.y);
        m_playerTimeData.Add(newTimeDataRecording);
        //Debug.Log("Redocding Data");
    }

    public void StartRecording()
    {
        ShouldRecordMovements = true;
    }

    public void SwitchToPlayback()
    {
        Instantiated_Clone = GameObject.Instantiate(CloneObjectPrefab, SpawnPosition.transform.position, Quaternion.identity);
        Instantiated_Clone.GetComponent<Clone_Object>().Spawned(PlayerObject, this);

        ShouldRecordMovements = false;

        ReplayMovements = true;
    }

    private void GiveGameObjectData(TimeData timeData)
    {
        Instantiated_Clone.transform.position = timeData.position;
        Instantiated_Clone.transform.rotation = Quaternion.Euler(0, timeData.zRotation, 0);
    }
}

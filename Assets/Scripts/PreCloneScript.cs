using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreCloneScript : MonoBehaviour
{

    [SerializeField]
    bool RespawnOnYPress;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (RespawnOnYPress)
        {
            if (Input.GetKeyDown(KeyCode.Y))
            {
                Game_Controller.Instance.SpawnClone();
                this.gameObject.SetActive(false);
            }
        }
    }
}

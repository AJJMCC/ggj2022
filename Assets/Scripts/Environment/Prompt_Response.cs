using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prompt_Response : MonoBehaviour
{

    public string PromptText;

    private void Start()
    {
        if (gameObject.tag != "Interact")
        {
            gameObject.tag = "Interact";
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitAfterTime : MonoBehaviour
{
    [SerializeField]
    private int TimeTillQuit;
    private float T = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        T += Time.deltaTime;
        if (T >= TimeTillQuit)
        {
            Debug.Log("Should have quit");
            Application.Quit();
            
        }
    }
}

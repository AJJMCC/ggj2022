using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Controller : MonoBehaviour
{
    public static UI_Controller Instance;

    [Header("PPE")]
    public GameObject PPEEquippedText;
  

    [Space(10)]
    public Text RegularPromptText;
    public GameObject ObjectiveText;


    [Space(10)]
    public GameObject CentreCursor;

    [Header("CloneDeathScreen")]
    public GameObject DeathScreen;
    public GameObject DeathScreenInGame;
    public Text DeathText;

    [Header("PreCLevel Screen")]
    public GameObject StartScreen;

    [Header("PreClone Scares")]
    public GameObject[] CloneSpawnScreens;

    [Header("Level Complete text")]
    public GameObject WinScreen;

    [Header("Level Lost Via Elect")]
    public GameObject ElectricutionScreen;

    [Header("Level Lost Via Time")]
    public GameObject TimeLostScreen;
    public Text TimerText;
    float LevelTime;
    float LT;
    [HideInInspector]
    public bool TrackingTme;

    [Header("Post Clone Effects")]
    public GameObject LetTherebeMonstersAudio;
    public RetroAesthetics.RetroCameraEffect CamToChange;

    public float glitchint = 0.1f;
    public int glitchfreq = 5;
    public int ChromAb = 5;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
       
    }


    public void ActivatePrompt(string InputPrompt)
    {
        RegularPromptText.text = InputPrompt;
    }

    public void DeactivatePrompt()
    {
        RegularPromptText.text = "";
    }

    public void DeactivateCursor()
    {
        CentreCursor.SetActive(false);
    }

    public void ActivateCursor()
    {
        CentreCursor.SetActive(true);
    }


    public void ActivatePreCLoneScreen()
    {
        CloneSpawnScreens[Game_Controller.Instance.CurrentRespawns].SetActive(true);
    }

    public void StopTrackingTime()
    {
        TrackingTme = false;
    }

    public void ResetTime()
    {
        LT = LevelTime;
        TrackingTme = true;
        IncreaseGlitchAmount();
    }
    public void DeactivateStartScreen()
    {
        ObjectiveText.SetActive(true);
        StartScreen.SetActive(false);
        LevelTime = Game_Controller.Instance.SecondsToDoLevel;
        TrackingTme = true;
        LT = LevelTime;
    }

    private void Update()
    {
        if (Interact_Controller.Instance.HasInsulationGloves)
        {
            PPEEquippedText.SetActive(true);
        }

        else
        {
            PPEEquippedText.SetActive(false);
        }


        if (TrackingTme)
        {
            TimerText.text = "Time: " + Mathf.RoundToInt(LT);
            LT -= Time.deltaTime;
            //Debug.Log("counting time");
        }

        if (LT <= 0 && LevelTime != 0 && TrackingTme)
        {
            Game_Controller.Instance.LostLevel();
            if (!TimeLostScreen.activeInHierarchy)
            {
                TrackingTme = false;
                TimerText.text = "Time: EXPIRED";
                TimeLostScreen.SetActive(true);
                
            }
        }
    }

    public void ActivateWinScreen()
    {
        WinScreen.SetActive(true);
    }

    public void ActivateElectrocuteScreen()
    {
        ElectricutionScreen.SetActive(true);
    }

    public void NullSUmmedUI()
    {
        DeathScreen.SetActive(true);
        DeathScreenInGame.SetActive(true);
    }

    public void IncreaseGlitchAmount()
    {
        LetTherebeMonstersAudio.SetActive(true);
        CamToChange.glitchIntensity = glitchint;
        CamToChange.glitchFrequency = glitchfreq;
        CamToChange.chromaticAberration = ChromAb;
    }

    


}

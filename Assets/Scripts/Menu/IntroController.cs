using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroController : MonoBehaviour
{
    [SerializeField]
    private bool PressY = true;

    [SerializeField]
    private bool PressN;

    [SerializeField]
    private GameObject NextTextY;

    [SerializeField]
    private GameObject NextTextN;


    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Y) && PressY)
        {
            SwitchToY();
        }

        if (Input.GetKeyDown(KeyCode.N) && PressN)
        {
            SwitchToN();
        }
    }

    void TurnYOn(int I)
    {
        if (NextTextY != null)
        {
            NextTextY.SetActive(true);
        }

        if (I == 1)
        {
            RemoveThis();
        }
       
    }

    void SwitchToY()
    {
        this.GetComponent<Text>().color = Color.clear;

        if (NextTextY != null)
        {
            NextTextY.SetActive(true);
        }

        RemoveThis();
    }
    void SwitchToN()
    {
        this.GetComponent<Text>().color = Color.clear;

        if (NextTextN != null)
        {
            NextTextN.SetActive(true);
        }

        RemoveThis();
    }

    void RemoveThis()
    {
        Destroy(gameObject);
    }

  
}

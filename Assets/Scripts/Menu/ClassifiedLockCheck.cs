using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ClassifiedLockCheck : MonoBehaviour
{

    
    private string UnlockBaconianCode;
    private string UnlockBaconianCode2;

    public GameObject InputFieldObj;
    private InputField OurPasscodeField;
    public GameObject ButtonToViewBrief;

    public GameObject BriefObj;
    public Text BriefIntro;


    public GameObject Menu;




    private void Awake()
    {
        OurPasscodeField = InputFieldObj.GetComponent<InputField>();
    }

    void Start()
    {
        UnlockBaconianCode = "AAABBBAABBAAAAAABABAABAAABAABABABBA";
        UnlockBaconianCode2 = "AAABB BAABB AAAAA ABABA ABAAA BAABA BABBA";
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Menu.SetActive(true);
            this.gameObject.SetActive(false);
        }
    }

    public void TextChanged()
    {
        if (OurPasscodeField.text == UnlockBaconianCode || OurPasscodeField.text == UnlockBaconianCode2)
        {
            Unlock();
        }
    }

    private void Unlock()
    {
        InputFieldObj.SetActive(false);
        ButtonToViewBrief.SetActive(true);


        Debug.Log("Unlocked");
    }

    public void ShowText()
    {
        BriefIntro.text = SetText();
        ButtonToViewBrief.SetActive(false);
        BriefObj.SetActive(true);
    }

    public string SetText()
    {
        string MadeUpString = "Good, " + CheckTheTime() + "officer";

        return MadeUpString;
    }

    public string CheckTheTime()
    {
        int sysHour = System.DateTime.Now.Hour;
        if (sysHour >= 12 && sysHour < 19)
        {
            return "afternoon ";
        }
        else if (sysHour >= 19 && sysHour < 24 || sysHour >= 0 && sysHour < 5)
        {
            return "evening ";
        }
        else if (sysHour >= 5 && sysHour < 12)
        {
            return "morning ";
        }
        else return "day ";

    }
}

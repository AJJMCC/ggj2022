using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game_Controller : MonoBehaviour
{
    public static Game_Controller Instance;

    [Header ("Player speed")]
    public float RegularWalkSpeed;
    public float RegularBackWalkSpeed;
    public float RegularStrafeSpeed;

    [Space(15)]
    public float RegularTurnSpeed;

    [Header("Player Object")]
    [Space(15)]
    public GameObject PlayerObj;

    [Header("Player respawn points")]
    [Space(15)]
    public Transform[] RespawnPoints;

    [Header("PPE")]
    public GameObject PPE;
    public CheckForLeftClone[] PPEPs;
    public Transform PPESpawn;

    [Header("Max and current clones")]
    public int RespawnMax = 2;
    public int CurrentRespawns = 0;

    [Header("Clone controllers")]
    public bool ClonesExistInThisLevel;
    public ClonePositionRecorder[] CloneControllers;


    [Header("Tutorial level changing")]
    public float TimeUntilYouCanChangeLevels = 18;
    public bool IsTutorial = false;
    float T; // used to compare to the float above

    [Header("Normal level changing")]
    public int LevelToLoad;

    public bool PlayerHasControl = false;

    [Header("LevelTimer")]
    public float SecondsToDoLevel;
    [HideInInspector]
    public bool LevelLostFromTime;





    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Start()
    {
        Instance = this;
        StopPlayerControl();
        UI_Controller.Instance.StartScreen.SetActive(true);
    }

    private void Update()
    {
        T += Time.deltaTime;
        if (T > TimeUntilYouCanChangeLevels && Input.GetKeyDown(KeyCode.Y)  && IsTutorial)
        {
            SceneManager.LoadScene(2);
        }

        if (Input.GetKeyDown(KeyCode.Escape) && PlayerHasControl)
        {
            SceneManager.LoadScene(0);
        }
    }
   
    public void StopPlayerControl()
    {
        PlayerObj.GetComponent<ECM.Controllers.BaseFirstPersonController>().forwardSpeed = 0;
        PlayerObj.GetComponent<ECM.Controllers.BaseFirstPersonController>().backwardSpeed = 0;
        PlayerObj.GetComponent<ECM.Controllers.BaseFirstPersonController>().strafeSpeed = 0;

        PlayerObj.GetComponent<ECM.Components.MouseLook>().lateralSensitivity = 0;
        PlayerObj.GetComponent<ECM.Components.MouseLook>().verticalSensitivity = 0;

        Interact_Controller.Instance.LookForInput = false;
        PlayerHasControl = false;
    }

    public void ResumePlayerControl()
    {
        PlayerObj.GetComponent<ECM.Controllers.BaseFirstPersonController>().forwardSpeed = RegularWalkSpeed;
        PlayerObj.GetComponent<ECM.Controllers.BaseFirstPersonController>().backwardSpeed = RegularBackWalkSpeed;
        PlayerObj.GetComponent<ECM.Controllers.BaseFirstPersonController>().strafeSpeed = RegularStrafeSpeed;

        PlayerObj.GetComponent<ECM.Components.MouseLook>().lateralSensitivity = RegularTurnSpeed;
        PlayerObj.GetComponent<ECM.Components.MouseLook>().verticalSensitivity = RegularTurnSpeed;

        Interact_Controller.Instance.LookForInput = true;
        PlayerHasControl = true;
    }

    public void LostLevel()
    {
        if (!LevelLostFromTime)
        {
            StopPlayerControl();
        }

        LevelLostFromTime = true;
        Invoke("ReloadLevelTotal", 7);
    }

    public void Electrocuted()
    {
        StopPlayerControl();
        Invoke("ReloadLevelTotal", 7);
    }

    public void NullSummed()
    {
        StopPlayerControl();
        UI_Controller.Instance.NullSUmmedUI();
        Invoke("ReloadLevelTotal", 3);
    }

    void ReloadLevelTotal()
    {
        int CurrentLevelInt = SceneManager.GetActiveScene().buildIndex;

        SceneManager.LoadScene(CurrentLevelInt);
    }

    public void CompletedLevel()
    {
        UI_Controller.Instance.StopTrackingTime();
        StopPlayerControl();
        Debug.Log("GM thinks player won");
        CloneControllers[0].ReplayMovements = false;

        Invoke("LoadNextLevel", 10);
    }

    void LoadNextLevel()
    {
        SceneManager.LoadScene(0);
    }

    // CLONE STUFF

    public void ShowPreCloneScreen()
    {
        StopPlayerControl();
        if (CurrentRespawns < RespawnMax)
        {
            UI_Controller.Instance.ActivatePreCLoneScreen();
        }

      
    }

    public void SpawnClone()
    {
        Debug.Log("Old respawn count " + CurrentRespawns);
        PlayerObj.transform.position = RespawnPoints[CurrentRespawns].position;
        ResumePlayerControl();
        //CloneControllers[CurrentRespawns].SpawnPosition = RespawnPoints[CurrentRespawns];
        CloneControllers[CurrentRespawns].SwitchToPlayback();
        UI_Controller.Instance.ResetTime();
        Interact_Controller.Instance.HasInsulationGloves = false;
        GameObject NewPPE = GameObject.Instantiate(PPE, PPESpawn.position, Quaternion.identity);

        foreach(CheckForLeftClone A in PPEPs)
        {
            A.TalkTO = NewPPE;
        }


        CurrentRespawns++;
        Debug.Log("New respawn count " + CurrentRespawns);
    }

    public void StartRecording()
    {
        if (ClonesExistInThisLevel)
        CloneControllers[CurrentRespawns].StartRecording();
    }


}

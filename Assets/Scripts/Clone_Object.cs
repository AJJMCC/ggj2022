using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clone_Object : MonoBehaviour
{
    [Header("Distance for instant notice")]
    public float DistanceBeforeTurn;

    [Header("Components of me")]
    public Animator CharacterAnimator;
    public GameObject Head;


    bool NoticedPlayer = false;

    public GameObject WarningLight;
    public float TimeForPlayerToBackOff = 2;
    float RT;

    GameObject PlayerObj;
    ClonePositionRecorder MyparentRecorder;

    bool Alive;
    // Start is called before the first frame update
    void Start()
    {
        RT = 0;
    }

    public void Spawned(GameObject PlayerParsedIn, ClonePositionRecorder parentrec)
    {
        PlayerObj = PlayerParsedIn;
        MyparentRecorder = parentrec;
        Alive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Alive)
        {
            float T = Vector3.Distance(PlayerObj.transform.position, this.transform.position);
            if (T <= DistanceBeforeTurn)
            {
                if (!NoticedPlayer)
                {
                    HasNoticedPlayerClose();
                    Debug.Log("NoticedPlayer");
                    NoticedPlayer = true;
                }

                else
                {
                    RT += Time.deltaTime;
                    if (RT > TimeForPlayerToBackOff)
                    {
                        KillPlayer();
                        Destroy(this.gameObject);
                    }
                }
            }

            else
            {
                if (NoticedPlayer)
                {
                    PlayerMovedAway();
                    NoticedPlayer = false;
                }

            }

            if (NoticedPlayer)
            {
                Head.transform.LookAt(PlayerObj.transform);
            }
        }
        
    }

    void HasNoticedPlayerClose()
    {
        //Debug.Log("Noticed Player");
        //if (CharacterAnimator.enabled)
        //CharacterAnimator.SetTrigger("StopWalking");
        MyparentRecorder.ReplayMovements = false;
        //Head.transform.LookAt(PlayerObj.transform);
        //Head.transform.rotation *= Quaternion.Euler(Head.transform.rotation.x, Head.transform.rotation.y + 90, Head.transform.rotation.z);
        WarningLight.SetActive(true);
    }

    void PlayerMovedAway()
    {
        //if (CharacterAnimator.enabled)
        //CharacterAnimator.SetTrigger("KeepWalking");
        RT = 0;
        NoticedPlayer = false;
        MyparentRecorder.ReplayMovements = true;
        WarningLight.SetActive(false);
    }

    void KillPlayer()
    {
        //if (CharacterAnimator.enabled)
        //CharacterAnimator.SetTrigger("StopWalking");
        WarningLight.SetActive(false);
        Alive = false;

        Game_Controller.Instance.NullSummed();

    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact_Controller : MonoBehaviour
{
    public static Interact_Controller Instance;

    // raycast variables
    public float lookrange = 2f;

    // camera to ray from
    private Camera cam;

    [HideInInspector]
    public bool LookForInput;

    public bool HasInsulationGloves;


    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        LookForInput = false;
        cam = Camera.main;
    }


    void Update()
    {
        if (LookForInput)
        InputCheck();
    }
    void InputCheck()
    {
        Ray ray = new Ray(cam.transform.position, cam.transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray,  out hit, lookrange))
        {
            if (hit.collider != null && hit.collider.tag == "Interact")
            {
                string I = hit.collider.GetComponent<Prompt_Response>().PromptText;
                UI_Controller.Instance.ActivatePrompt(I);

                if (Input.GetMouseButtonDown(0))
                {
                    Debug.Log("Interacted with X");
                    if (hit.collider.GetComponent<Clone_Activater>())
                    {
                        hit.collider.GetComponent<Clone_Activater>().ActivatedByPlayer();
                    }
                    else if (hit.collider.GetComponent<Level_Won_Interact>())
                    {
                        hit.collider.GetComponent<Level_Won_Interact>().ActivatedByPlayer();
                    }

                    else if (hit.collider.GetComponent<EquipPPE>())
                    {
                        hit.collider.GetComponent<EquipPPE>().ActivatedByPlayer();
                    }
                }
            }

            else
            {
                UI_Controller.Instance.DeactivatePrompt();
            }
        }

        else
        {
            UI_Controller.Instance.DeactivatePrompt();
        }
    }
}

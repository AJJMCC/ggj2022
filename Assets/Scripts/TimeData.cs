﻿using UnityEngine;

public class TimeData
{
	public Vector3 position;
	public float zRotation;
	
	public TimeData()
	{
		position = Vector3.zero;
		zRotation = 0.0f;
	}

	public TimeData(Vector3 _position, float _angleAim)
	{
		position = _position;
		zRotation = _angleAim;
	}
}

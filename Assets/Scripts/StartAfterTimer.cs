using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAfterTimer : MonoBehaviour
{
    [SerializeField]
    bool StartOnYPress;

    [SerializeField]
    bool TurnOnAudio;
    [SerializeField]
    GameObject Audio;




    [SerializeField]
    bool StartRecordingonYPress;

   [SerializeField]
    float TimeUntilStart;

    float T = 0;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        T += Time.deltaTime;

        if (T > TimeUntilStart)
        {
            if (StartOnYPress)
            {
                if (Input.GetKeyDown(KeyCode.Y))
                {
                    Game_Controller.Instance.ResumePlayerControl();
                    UI_Controller.Instance.DeactivateStartScreen();
                    Debug.Log("Started Player Control");
                    if (StartRecordingonYPress)
                    {
                        Game_Controller.Instance.StartRecording();
                    }

                    if(TurnOnAudio)
                    {
                        Audio.SetActive(true);
                    }
                        
                }
            }

           
        }

        
    }
}

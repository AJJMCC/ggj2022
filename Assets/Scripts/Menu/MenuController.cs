using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    [Header ("classified stuff")]
    public GameObject LockScreenForClassified;

    [Header("info screens")]
    public GameObject SiMInfoScreen;
    public GameObject DeptInfoScreen;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }
    public void TurnOnClassifiedLockScreen()
    {
        LockScreenForClassified.SetActive(true);
    }

    public void TurnOffClassifiedLockScreen()
    {
        LockScreenForClassified.SetActive(false);
    }

    public void BeginGame()
    {
        Application.LoadLevel(1);
    }

    public void ShowInfoScreen(int WhichScreen)
    {
        if (WhichScreen == 1)
        {
            if (SiMInfoScreen != null)
                SiMInfoScreen.SetActive(true);
            Debug.Log("Sim Info Should Be Active");
        }

        if (WhichScreen == 2)
        {
            if (DeptInfoScreen != null)
                DeptInfoScreen.SetActive(true);
            Debug.Log("dept Info Should Be Active");
        }
    }

    public void MakeShiftQuit()
    {
        Debug.Log("Button Quit");
        Application.Quit();
    }
}

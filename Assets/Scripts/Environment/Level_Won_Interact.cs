using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_Won_Interact : MonoBehaviour
{
    public void ActivatedByPlayer()
    {
        Game_Controller.Instance.CompletedLevel();
        UI_Controller.Instance.ActivateWinScreen();
    }
}
